const express = require('express')
const cors = require('cors')
var fs = require('fs');
const bodyParser = require('body-parser');
const app = express()
app.use(cors())
app.use(bodyParser.urlencoded({ extended: true }));


app.get('/', (req, res, next) => {
    res.send(`<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Document</title>
    </head>
    <body>
        <form method="post" action="http://localhost:4000/register">
            <label for="firstname">First name:</label>
            <input type="text" name="firstname" /><br />
            <label for="lastname">Last name:</label>
            <input type="text" name="lastname" /><br />
            <input type="submit" />
        </form>
    </body>
    </html>`)
})

app.post('/register', (req, res, next) => {
    fs.appendFile('data.txt', `${req.body.firstname},${req.body.lastname}\n`, function (err) {
        if (err) res.send(e);
        res.send('submitted')
    });
})

app.get('/readData', (req, res, next) => {
    fs.readFile('data.txt', 'utf8', (err, data) => {
        if (err) {
            res.send("failed to read");
            return;
        }
        res.send(`<pre>${data}<pre>`)
    });
})

fs.writeFile('data.txt', "", (err) => {
    if (err) {
        console.error('Error creating the file:', err);
        return;
    }
    console.log('File created successfully!');
});

app.listen(4000, () => {
    console.log("listening to port 4000")
})