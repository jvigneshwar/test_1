Steps to setup the CI CD pipeline in GitLab:
1: Create a repo in gitlab and clone it to the local
2: Create a Node app and push back to the remote.
3: Create a .gitlab-ci.yml file in gitlab and give the necessary steps to build and deploy.
4: Create different steps, like the build stage and the deploy stage.
5: Give the necessary scripts to install and run the Node application.
6: Add the artefacts to cache the node modules and package.lock.json in the yml file.
7: Set the image to node to avoid installing node.
8: Save the yml file or commit, and then the pipeline will be created and deployed.
